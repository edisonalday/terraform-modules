# AWS EC2 Instance Terraform module

Terraform module which creates EC2 instance(s) on AWS.

These types of resources are supported:

* [EC2 instance](https://www.terraform.io/docs/providers/aws/r/instance.html)

## Usage

To run this example you need to execute:

```bash
$ terraform init
$ terraform plan
$ terraform apply
```

## Notes

Note that this example may create resources which cost money. Run `terraform destroy` when you don't need these resources.

* `network_interface` can't be specified together with `associate_public_ip_address`, which makes `network_interface` not configurable using this module at the moment.
* Changes in `ebs_block_device` argument will be ignored. Use [aws_volume_attachment](https://www.terraform.io/docs/providers/aws/r/volume_attachment.html) resource to attach and detach volumes from AWS EC2 instances. See [this example](https://github.com/terraform-aws-modules/terraform-aws-ec2-instance/tree/master/examples/volume-attachment).
* One of `subnet_id` or `subnet_ids` is required. If both are provided, the value of `subnet_id` is prepended to the value of `subnet_ids`.


## Required Inputs

The following input variables are required:

### ami

Description: Amazon Machine ID used for provisioning the instance

Type: `string`

Default: `"ami-06e7b9c5e0c4dd014"`

### instance\_type

Description: Type of instance to be provisioned

Type: `string`

Default: `"t2.micro"`

### name

Description: Tag the instance with this name

Type: `string`

Default: `"terraform-instance"`

### vpc\_security\_group\_ids

Description: Subnet ID where the isntance has to be created

Type: `list`

Default: `<list>`

## Optional Inputs

The following input variables are optional (have default values):

### block\_device\_names

Description: Enter the block device name (refer to aws doc)- the total default names should be equal to the ebs_count value

Type: `string`

Default: `"xvdh"`

### ebs\_count

Description: Enter the number EBS volumes to attach to the instance

Type: `string`

Default: `"2"`

### ebs\_size

Description: Size of the EBS volumes. This has to be consisten with EBS_count and block_device_names

Type: `string`

Default: `"100"`

### ebs\_type

Description: EBS type

Type: `string`

Default: `"gp2"`

### iam\_instance\_profile

Description: iam role to enable ec2 to access s3

Type: `string`

Default: `"Full_S3_access_from_EC2"`

### instance\_count

Description: Number of amazon instance to be provisioned

Type: `string`

Default: `"2"`

### key\_name

Description: Type key to be used for ssh

Type: `string`

Default: `""`

### subnet\_ids

Description: List of subnets to be associated with the instance

Type: `list`

Default: `<list>`

## Outputs

### availability\_zone

Description: List of availability zones of instances

### credit\_specification

Description: List of credit specification of instances

### id

Description: List of IDs of instances

### key\_name

Description: List of key names of instances

### primary\_network\_interface\_id

Description: List of IDs of the primary network interface of instances

### private\_dns

Description: List of private DNS names assigned to the instances. Can only be used inside the Amazon EC2, and only available if you've enabled DNS hostnames for your VPC

### private\_ip

Description: List of private IP addresses assigned to the instances

### public\_dns

Description: List of public DNS names assigned to the instances. For EC2-VPC, this is only available if you've enabled DNS hostnames for your VPC

### public\_ip

Description: List of public IP addresses assigned to the instances, if applicable

### security\_groups

Description: List of associated security groups of instances

### subnet\_id

Description: List of IDs of VPC subnets of instances

### tags

Description: List of tags of instances

### vpc\_security\_group\_ids

Description: List of associated security groups of instances, if running in non-default VPC