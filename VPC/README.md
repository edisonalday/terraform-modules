# AWS VPC Terraform Module 

Terraform module which creates VPC resources on AWS.

These types of resources are supported:

* [VPC](https://www.terraform.io/docs/providers/aws/r/vpc.html)
* [Subnet](https://www.terraform.io/docs/providers/aws/r/subnet.html)
* [Route](https://www.terraform.io/docs/providers/aws/r/route.html)
* [Route table](https://www.terraform.io/docs/providers/aws/r/route_table.html)
* [Internet Gateway](https://www.terraform.io/docs/providers/aws/r/internet_gateway.html)
* [Network ACL](https://www.terraform.io/docs/providers/aws/r/network_acl.html)
* [NAT Gateway](https://www.terraform.io/docs/providers/aws/r/nat_gateway.html)
* [VPN Gateway](https://www.terraform.io/docs/providers/aws/r/vpn_gateway.html)

## Usage

Call the vpc module and specify a valid CIDR. By default, if NAT Gateways are enabled, private subnets will be configured with routes for Internet traffic that point at the NAT Gateways configured by use of the above options.

If you need private subnets that should have no Internet routing intra_subnets should be used instead.

## Notes

Note that this example may create resources which cost money. Run `terraform destroy` when you don't need these resources.

## Required Inputs

### azs

Description: A list of availability zones in the region

Type: `list`

Default: `[]`

### cidr

Description: The CIDR block for the VPC.

Type: `string`

Default: `"10.0.0.0/16"`

### name

Description: Name to be used on all the resources as identifier

Type: `string`

Default: `"custom-vpc"`

### private\_subnets

Description: A list of private subnets inside the VPC

Type: `list`

Default: `[]`

### public\_subnets

Description: A list of public subnets inside the VPC

Type: `list`

Default: `[]`

The following input variables are required:

## Optional Inputs

The following input variables are optional (have default values):

### tag\_name

Description: Tag name to be assigned to the instance

Type: `string`

Default: `"dev"`

### tag\_company

Description: Tag company to be assigned to the instance

Type: `string`

Default: `"My-VPC"`

## Outputs

The following outputs are exported:

### intra\_subnets

Description: List of IDs of intra subnets

### public\_subnets

Description: List of IDs of public subnets

### vpc\_id

Description: The ID of the VPC