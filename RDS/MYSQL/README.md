# AWS RDS Terraform module for RDS

Terraform module which creates RDS resources on AWS.

These types of resources are supported:

* [DB Instance](https://www.terraform.io/docs/providers/aws/r/db_instance.html)
* [DB Subnet Group](https://www.terraform.io/docs/providers/aws/r/db_subnet_group.html)
* [DB Parameter Group](https://www.terraform.io/docs/providers/aws/r/db_parameter_group.html)
* [DB Option Group](https://www.terraform.io/docs/providers/aws/r/db_option_group.html)

Root module calls these modules which can also be used separately to create independent resources:

* [db_instance](https://github.com/terraform-aws-modules/terraform-aws-rds/tree/master/modules/db_instance) - creates RDS DB instance
* [db_subnet_group](https://github.com/terraform-aws-modules/terraform-aws-rds/tree/master/modules/db_subnet_group) - creates RDS DB subnet group
* [db_parameter_group](https://github.com/terraform-aws-modules/terraform-aws-rds/tree/master/modules/db_parameter_group) - creates RDS DB parameter group
* [db_option_group](https://github.com/terraform-aws-modules/terraform-aws-rds/tree/master/modules/db_option_group) - creates RDS DB option group

## Usage

To run this example you need to execute:

```bash
$ terraform init
$ terraform plan
$ terraform apply
```

## Notes

Note that this example may create resources which cost money. Run `terraform destroy` when you don't need these resources.

## Required Inputs

### allocated\_storage

Description: The allocated storage in gigabytes.

Type: `string`

Default: `""`

### identifier

Description: The name of the RDS instance, if omitted, Terraform will assign a random, unique identifier.

Type: `string`

Default: `""`

### instance\_class

Description: The instance type of the RDS instance.

Type: `string`

Default: `""`

### password

Description: Password for the master DB user. Note that this may show up in logs, and it will be stored in the state file.

Type: `string`

Default: `""`

### subnet\_ids

Description: A list of VPC subnet IDs.

Type: `list`

Default: `<list>`

### username

Description: Username for the master DB user.

Type: `string`

Default: `""`

### vpc\_security\_group\_ids

Description: List of VPC security groups to associate.

Type: `list`

Default: `<list>`

## Fixed Inputs

### allow\_major\_version\_upgrade

Description: Indicates that major version upgrades are allowed. Changing this parameter does not result in an outage and the change is asynchronously applied as soon as possible.

Type: `string`

Default: `"false"`


### auto\_minor\_version\_upgrade

Description: Indicates that minor engine upgrades will be applied automatically to the DB instance during the maintenance window.

Type: `string`

Default: `"true"`


### backup\_retention\_period

Description: The days to retain backups for.

Type: `string`

Default: `"1"`


### backup\_window

Description: The daily time range (in UTC) during which automated backups are created if they are enabled. Example: '09:46-10:16'. Must not overlap with maintenance_window.

Type: `string`

Default: `""`

### deletion\_protection

Description: The database can't be deleted when this value is set to true.

Type: `string`

Default: `"false"`

### maintenance\_window

Description: The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi'. Eg: 'Mon:00:00-Mon:03:00'.

Type: `string`

Default: `""`

### port

Description: The port on which the DB accepts connections.

Type: `string`

Default: `""`


### publicly\_accessible

Description: Bool to control if instance is publicly accessible.

Type: `string`

Default: `"false"`


### storage\_encrypted

Description: Specifies whether the DB instance is encrypted.

Type: `string`

Default: `"false"`


### storage\_type

Description: One of 'standard' (magnetic), 'gp2' (general purpose SSD), or 'io1' (provisioned IOPS SSD). The default is 'io1' if iops is specified, 'standard' if not. Note that this behaviour is different from the AWS web console, where the default is 'gp2'.

Type: `string`

Default: `"gp2"`

### timezone

Description: Time zone of the DB instance. Timezone is currently only supported by Microsoft SQL Server.

Type: `string`

Default: `""`

## Optional Inputs

### availability\_zone

Description: The Availability Zone of the RDS instance.

Type: `string`

Default: `""`

### copy\_tags\_to\_snapshot

Description: On delete, copy all Instance tags to the final snapshot (if final_snapshot_identifier is specified).

Type: `string`

Default: `"false"`

### create\_monitoring\_role

Description: Create IAM role with a defined name that permits RDS to send enhanced monitoring metrics to CloudWatch Logs.

Type: `string`

Default: `"false"`

### db\_subnet\_group\_name

Description: Name of DB subnet group. DB instance will be created in the VPC associated with the DB subnet group. If unspecified, will be created in the default VPC.

Type: `string`

Default: `""`

### enabled\_cloudwatch\_logs\_exports

Description: List of log types to enable for exporting to CloudWatch logs. If omitted, no logs will be exported. Valid values (depending on engine): alert, audit, error, general, listener, slowquery, trace, postgresql (PostgreSQL), upgrade (PostgreSQL).

Type: `list`

Default: `<list>`

### engine

Description: The database engine to use.

Type: `string`

Default: `""`

### engine\_version

Description: The engine version to use.

Type: `string`

Default: `""`

### family

Description: The family of the DB parameter group. 

Type: `string`

Default: `""`

### final\_snapshot\_identifier

Description: The name of your final DB snapshot when this DB instance is deleted.

Type: `string`

Default: `"false"`

### iam\_database\_authentication\_enabled

Description: Specifies whether or mappings of AWS Identity and Access Management (IAM) accounts to database accounts is enabled.

Type: `string`

Default: `"false"`

### iops

Description: The amount of provisioned IOPS. Setting this implies a storage_type of 'io1'.

Type: `string`

Default: `"0"`

### kms\_key\_id

Description: The ARN for the KMS encryption key. If creating an encrypted replica, set this to the destination KMS ARN. If storage_encrypted is set to true and kms_key_id is not specified the default KMS key created in your account will be used.

Type: `string`

Default: `""`

### license\_model

Description: License model information for this DB instance. Optional, but required for some DB engines, i.e. Oracle SE1.

Type: `string`

Default: `""`

### major\_engine\_version

Description: Specifies the major version of the engine that this option group should be associated with.

Type: `string`

Default: `""`

### monitoring\_interval

Description: The interval, in seconds, between points when Enhanced Monitoring metrics are collected for the DB instance. To disable collecting Enhanced Monitoring metrics, specify 0. The default is 0. Valid Values: 0, 1, 5, 10, 15, 30, 60.

Type: `string`

Default: `"0"`

### monitoring\_role\_arn

Description: The ARN for the IAM role that permits RDS to send enhanced monitoring metrics to CloudWatch Logs. Must be specified if monitoring_interval is non-zero.

Type: `string`

Default: `""`

### monitoring\_role\_name

Description: Name of the IAM role which will be created when create_monitoring_role is enabled.

Type: `string`

Default: `"rds-monitoring-role"`

### multi\_az

Description: Specifies if the RDS instance is multi-AZ.

Type: `string`

Default: `"false"`

### name

Description: The DB name to create. If omitted, no database is created initially.

Type: `string`

Default: `""`

### option\_group\_description

Description: The description of the option group.

Type: `string`

Default: `""`

### option\_group\_name

Description: Name of the DB option group to associate. Setting this automatically disables option_group creation.

Type: `string`

Default: `""`

### options

Description: A list of Options to apply.

Type: `list`

Default: `<list>`

### parameter\_group\_description

Description: Description of the DB parameter group to create.

Type: `string`

Default: `""`

### parameter\_group\_name

Description: Name of the DB parameter group to associate or create.

Type: `string`

Default: `""`

### parameters

Description: A list of DB parameters (map) to apply.

Type: `list`

Default: `<list>`

### replicate\_source\_db

Description: Specifies that this resource is a Replicate database, and to use this value as the source database. This correlates to the identifier of another Amazon RDS Database to replicate.

Type: `string`

Default: `""`

### skip\_final\_snapshot

Description: Determines whether a final DB snapshot is created before the DB instance is deleted. If true is specified, no DBSnapshot is created. If false is specified, a DB snapshot is created before the DB instance is deleted, using the value from final_snapshot_identifier.

Type: `string`

Default: `"true"`

### tags

Description: A mapping of tags to assign to all resources.

Type: `map`

Default: `<map>`

### timeouts

Description: Updated Terraform resource management timeouts. Applies to `aws_db_instance` in particular to permit resource management times.

Type: `map`

Default: `<map>`

### use\_parameter\_group\_name\_prefix

Description: Whether to use the parameter group name prefix or not.

Type: `string`

Default: `"true"`

## Outputs

### this\_db\_instance\_address

Description: The address of the RDS instance.

### this\_db\_instance\_arn

Description: The ARN of the RDS instance.

### this\_db\_instance\_availability\_zone

Description: The availability zone of the RDS instance.

### this\_db\_instance\_endpoint

Description: The connection endpoint.

### this\_db\_instance\_hosted\_zone\_id

Description: The canonical hosted zone ID of the DB instance (to be used in a Route 53 Alias record).

### this\_db\_instance\_id

Description: The RDS instance ID.

### this\_db\_instance\_name

Description: The database name.

### this\_db\_instance\_password

Description: The database password (this password may be old, because Terraform doesn't track it after initial creation).

### this\_db\_instance\_port

Description: The database port.

### this\_db\_instance\_resource\_id

Description: The RDS Resource ID of this instance.

### this\_db\_instance\_status

Description: The RDS instance status.

### this\_db\_instance\_username

Description: The master username for the database.

### this\_db\_option\_group\_arn

Description: The ARN of the db option group.

### this\_db\_option\_group\_id

Description: The db option group id.

### this\_db\_parameter\_group\_arn

Description: The ARN of the db parameter group.

### this\_db\_parameter\_group\_id

Description: The db parameter group id.

### this\_db\_subnet\_group\_arn

Description: The ARN of the db subnet group.

### this\_db\_subnet\_group\_id

Description: The db subnet group name.